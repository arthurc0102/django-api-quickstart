from django.db import models


class User(models.Model):
    account = models.CharField(max_length=255)
    password = models.CharField(max_length=255)

    def __str__(self):
        return self.account

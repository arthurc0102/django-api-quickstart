from .common import *
from .config import ALLOWED_HOSTS
from .secret_key import SECRET_KEY

import os
import random

from .common import SETTINGS_DIR


key_file_path = os.path.join(SETTINGS_DIR, 'key.txt')
key = None

try:
    file = open(key_file_path, 'r')
    key = file.read().strip()
    file.close()
except FileNotFoundError:
    key = ''.join([random.SystemRandom().choice('abcdefghijklmnopqrstuvwxyz0123456789!@#$%^&*(-_=+)') for i in range(50)])
    file = open(key_file_path, 'w+')
    file.write(key)
    file.close()
finally:
    if key is None:
        Exception('Please gen secret file by yourself.')

    # SECURITY WARNING: keep the secret key used in production secret!
    SECRET_KEY = key
